<div class="form-group">
        {!! Form::label('name','Nombre') !!}
        {!! Form::text('name',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('ubicacion','Ubicacion') !!}
        {!! Form::text('ubicacion',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('cp','Codigo Postal') !!}
        {!! Form::text('cp',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('telefono','Telefono') !!}
        {!! Form::text('telefono',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('dirAdmin','Dirección de la administración') !!}
        {!! Form::text('dirAdmin',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('email','Correo electrónico') !!}
        {!! Form::text('email',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('avatar','Imagen/Logotipo') !!} 
        {!! Form::file('avatar') !!}
    </div>