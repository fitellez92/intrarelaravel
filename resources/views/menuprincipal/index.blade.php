<!--Hereda del archivo app.blade-->
@extends('layouts.app')

@section('title','Formulario de ....')
<!--include('layouts.menu') -->
@section('content')
	<div class="row">
		<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

		<div class="col-sm">
			<div class="card text-center" style="width: 8rem;margin-top: 30px">
				<a href="{{ action('PrivadaController@index') }}" class="btn btn-primary">Privadas</a>
			</div>
			<div class="card text-center" style="width: 10rem;margin-top: 30px">
				<a href="{{ action('AdministradoresController@index') }}" class="btn btn-success">Administradores</a>
			</div>
			<div class="card text-center" style="width: 10rem;margin-top: 30px">
				<a href="{{ action('AsignacionesController@index') }}" class="btn btn-info">Asignaciones</a>
			</div>
			<div class="card text-center" style="width: 10rem;margin-top: 30px">
				<a href="{{ action('PrivadaController@index') }}" class="btn btn-warning">Ocupaciones</a>
			</div>
		</div>
	</div>
		   				
@endsection