<div class="form-group">
        {!! Form::label('name','Nombre') !!}
        {!! Form::text('name',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('apellido','Apellidos') !!}
        {!! Form::text('apellido',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('telefono','Telefono') !!}
        {!! Form::text('telefono',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('email','Correo electrónico') !!}
        {!! Form::text('email',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('pass','Contraseña') !!}
        {!! Form::text('pass',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('pass2','Confirmar contraseña') !!} 
        {!! Form::text('pass2',null,['class'=>'form-control']) !!}
    </div>