<!--Hereda del archivo app.blade-->
@extends('layouts.app')

@section('title','Nueva Asignacion')
@section('encabezado','Nueva Asignacion')

@section('content')

{!! Form::open(['route'=>'asignaciones.store', 'method'=>'POST','files'=>true]) !!}
    <!--Se incluye en create.blade y edit.blade, ya que ambos son los mismos formularios y se pueden usar como un subview -->
	@include('asignaciones.form')
	<a href="{{ action('AsignacionesController@index') }}" class="btn btn-danger">Regresar</a>
    {!!Form::submit('Guardar',['class' => 'btn-primary'])!!}
{!! Form::close() !!}
<!--
<form class="form-group" method="POST" action="/trainers" enctype="multipart/form-data">
	
	<div class="form-group">
		<label>Nombre</label>
		<input type="text" name="name" class="form-control">
	</div>
	<div class="form-group">
		<label>Avatar</label>
		<input type="file" name="avatar">
	</div>
	<button type="submit" class="btn btn-primary">Guardar</button>
</form>

@endsection

<!--
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
	<body>
		<div class="container">
			<div class="form-group">
				<label>Nombre</label>
				<input type="text" class="form-control">
			</div>
			<button type="submit" class="btn btn-primary">Guardar</button>
	</div>
	</body>
</html>
-->