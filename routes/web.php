<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/intrare/agregar_privada', function () {
    return ('formulario');
});

Route::resource('intrare','IntrareController');
Route::resource('privada','PrivadaController');
Route::resource('administradores','AdministradoresController');
Route::resource('asignaciones','AsignacionesController');
